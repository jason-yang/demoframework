<?php
use Jason\Framework\Http\Request;

$start = microtime(true);
define('DEBUG', true);

/**
 * This is the starting point for all requests to the web application, except for static file requests.
 * This demo is loosely based on Symfony2 and other frameworks I have worked with previously.
 * 
 * @author Jason Yang
 */

function my_exception_handler($e)
{
	echo 'Sorry, there was an exception and we apologise for this!';
	exit;
}

function my_error_handler($errno, $errstr, $errfile, $errline)
{
	echo 'Sorry, there was an error and we apologise for this!';
	exit;
}

/**
 * Things to when debugging, and things to do when not debugging 
 */
if (DEBUG) {
	
} else {
	
	set_exception_handler('my_exception_handler');
	set_error_handler('my_error_handler');
}

// Using SplClassLoader from https://gist.github.com/221634
require(dirname(__FILE__) . '/../lib/SplClassLoader.php');

// Register namespace for Framework code
$fw_loader = new SplClassLoader('Jason\Framework', dirname(__FILE__) . '/../lib/');
$fw_loader->register();

// Register namespace for application code
$astral_loader = new SplClassLoader('Demo', dirname(__FILE__) . '/../app/');
$astral_loader->register();

$request = Request::CreateFromGlobals();

$application = Demo\App::instance();//new Demo\App();
$response = $application->handle($request);
$response->send();


if (DEBUG) {
?>
<div class="debug-bar">
	Runtime: <?php echo (microtime(true) - $start) * 1000 ?>ms
</div>
<?php
}