<?php
namespace Jason\Framework;

/**
 * Stores the Configuration for a particular Application
 * 
 * @author Jason
 */
class Configuration
{
	protected $key;
	protected $filename;
	protected $title;
	protected $databases;
	protected $session;
	
	/**
	 * Constructor
	 * 
	 * @param unknown $key
	 * @param unknown $filename
	 */
	public function __construct($key, $filename)
	{
		$this->key = $key;
		$this->filename = $filename;
		
		$this->init();
	}
	
	protected function init()
	{
		$config = simplexml_load_file($this->filename, 'SimpleXMLElement', LIBXML_NOCDATA);
		
		$this->title = (string) $config->title;
		
		if (isset($config->session)) {
			if ('true' == strtolower(trim((string) $config->session->enabled))) {
				$this->session = array(
						'model' => (string) $config->session->model,
						);
			}
		}

		if (isset($config->databases)) {
			$this->databases = array();
			foreach ($config->databases->database as $key => $db) {
				$this->databases[(string) $db['id']] = $db;
			}
		}
	}
	
	public function getKey()
	{
		return $key;
	}
	
	public function getFilename()
	{
		return $filename;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function getSession()
	{
		return $this->session;
	}
	
	public function getDatabases()
	{
		return $this->databases;
	}
	
}