<?php
namespace Jason\Framework\Controller;

use Jason\Framework\Application;

use Jason\Framework\View\View;

use Jason\Framework\Http\Response;


use Jason\Framework\Configuration;

use Jason\Framework\Http\Request;

class Controller
{
	protected $request;
	
	public function __construct(Request $request)
	{
		$this->request = $request;
	}
	
	public function renderView($viewClass, $options = array(), $status = 200, $headers = array())
	{
		$view = new $viewClass($options); /* @var $view View */
		
		$view->set('session', $this->getSession());
		
		return new Response($view, $status, $headers);
	}
	
	public function redirect($url)
	{
		header('Location: ' . $url);
		Application::terminate();
	}
	
	/**
	 * 
	 * @return Request
	 */
	public function getRequest()
	{
		return $this->request;
	}
	
	public function getSession()
	{
		return Application::instance()->getSession();
	}
	
	public function getConfiguration()
	{
		return Application::instance()->getConfiguration();
	}
	
	public function getTitle($append = null, $separator = ' - ', $override = false)
	{
		$title = $override ? '' : $this->getConfiguration()->getTitle();
		
		if ($append) {
			if (is_array($append)) {
				foreach ($append as $val) {
					$title .= $separator . $val;
				}
			} elseif (is_string($append)) {
				$title .= $separator . $append;
			} else {
				// DO NOTHING
			}
		}
		
		return $title;
	}
}