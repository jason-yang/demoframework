<?php
namespace Jason\Framework\Http;

class Parameters implements \ArrayAccess, \Countable, \Iterator
{
	private $_data;
	
	public function __construct(array $values)
	{
		$this->_data = $values;
	}
	
	public function get($index, $default = null)
	{
		return isset($this->_data[$index]) ? $this->_data[$index] : $default;
	}

	/**
	 * @see \ArrayAccess::offsetExists
	 * @param boolean $offset
	 */
	public function offsetExists($offset)
	{
		return isset($this->_data[$offset]);
	}
	
	/**
	 * @see \ArrayAccess::offsetGet
	 * @param mixed $offset
	 */
	public function offsetGet($offset)
	{
		return $this->_data[$offset];
	}
	
	/**
	 * @see \ArrayAccess::offsetSet
	 * @param mixed $offset
	 * @param mixed $value
	 */
	public function offsetSet($offset, $value)
	{
		$this->_data[$offset] = $value;
	}
	
	/**
	 * @see \ArrayAccess::offsetUnset
	 * @param unknown $offset
	 */
	public function offsetUnset($offset)
	{
		unset($this->_data[$offset]);
	}
	
	/**
	 * @see \Countable::count()
	 * @return number
	 */
	public function count()
	{
		return count($this->_data);
	}
	
	public function current()
	{
		return current($this->_data);
	}
	
	public function next ()
	{
		return next($this->_data);
	}
	
	public function key()
	{
		return key($this->_data);
	}
	
	public function valid ()
	{
		return current($this->_data) !== false;
	}
	
	public function rewind()
	{
		reset($this->_data);
	}
}
