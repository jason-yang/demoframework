<?php
namespace Jason\Framework\Http;

class Response
{
	protected $headers = array();
	protected $content;
	protected $version = '1.0';
	protected $statusCode;
	protected $statusText;
	protected $charset;
	
	/**
	 * Status codes translation table. (Note: This is copied from Symfony 2 codebase)
	 *
	 * The list of codes is complete according to the
	 * {@link http://www.iana.org/assignments/http-status-codes/ Hypertext Transfer Protocol (HTTP) Status Code Registry}
	 * (last updated 2012-02-13).
	 *
	 * Unless otherwise noted, the status code is defined in RFC2616.
	 *
	 * @var array
	 */
	public static $statusTexts = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			102 => 'Processing',            // RFC2518
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			207 => 'Multi-Status',          // RFC4918
			208 => 'Already Reported',      // RFC5842
			226 => 'IM Used',               // RFC3229
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => 'Reserved',
			307 => 'Temporary Redirect',
			308 => 'Permanent Redirect',    // RFC-reschke-http-status-308-07
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			418 => 'I\'m a teapot',                                               // RFC2324
			422 => 'Unprocessable Entity',                                        // RFC4918
			423 => 'Locked',                                                      // RFC4918
			424 => 'Failed Dependency',                                           // RFC4918
			425 => 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
			426 => 'Upgrade Required',                                            // RFC2817
			428 => 'Precondition Required',                                       // RFC6585
			429 => 'Too Many Requests',                                           // RFC6585
			431 => 'Request Header Fields Too Large',                             // RFC6585
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			506 => 'Variant Also Negotiates (Experimental)',                      // RFC2295
			507 => 'Insufficient Storage',                                        // RFC4918
			508 => 'Loop Detected',                                               // RFC5842
			510 => 'Not Extended',                                                // RFC2774
			511 => 'Network Authentication Required',                             // RFC6585
	);
	
	public function __construct($content = '', $status = 200, $headers = array())
	{
		$this->headers		= $headers;
		$this->content		= $content;
		$this->setStatusCode($status);
	}

    /**
     * Sets the response content. (Note: This is copied from Symfony 2 codebase)
     *
     * Valid types are strings, numbers, and objects that implement a __toString() method.
     *
     * @param mixed $content
     *
     * @return Response
     */
    public function setContent($content)
    {
        if (null !== $content && !is_string($content) && !is_numeric($content) && !is_callable(array($content, '__toString'))) {
            throw new \UnexpectedValueException('The Response content must be a string or object implementing __toString(), "'.gettype($content).'" given.');
        }

        $this->content = (string) $content;

        return $this;
    }

    /**
     * Gets the current response content.
     *
     * @return string Content
     */
    public function getContent()
    {
        return $this->content;
    }
	
	/**
	 * Sets the response status code. (Note: This is copied from Symfony 2 codebase)
	 *
	 * @param integer $code HTTP status code
	 * @param mixed   $text HTTP status text
	 *
	 * If the status text is null it will be automatically populated for the known
	 * status codes and left empty otherwise.
	 *
	 * @return Response
	 *
	 * @throws \InvalidArgumentException When the HTTP status code is not valid
	 */
	public function setStatusCode($code, $text = null)
	{
		$this->statusCode = $code = (int) $code;
		if ($this->isInvalid()) {
			throw new \InvalidArgumentException(sprintf('The HTTP status code "%s" is not valid.', $code));
		}
	
		if (null === $text) {
			$this->statusText = isset(self::$statusTexts[$code]) ? self::$statusTexts[$code] : '';
	
			return $this;
		}
	
		if (false === $text) {
			$this->statusText = '';
	
			return $this;
		}
	
		$this->statusText = $text;
	
		return $this;
	}

    /**
     * Retrieves the status code for the current web response.
     *
     * @return string Status code
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
    
    /**
     * Sends HTTP headers.
     *
     * @return Response
     */
    public function sendHeaders()
    {
        // headers have already been sent by the developer
        if (headers_sent()) {
            return $this;
        }

        // status
        header(sprintf('HTTP/%s %s %s', $this->version, $this->statusCode, $this->statusText));

        // headers
        foreach ($this->headers as $name => $values) {
            foreach ($values as $value) {
                header($name.': '.$value, false);
            }
        }

        return $this;
    }

    /**
     * Sends content for the current web response.
     *
     * @return Response
     */
    public function sendContent()
    {
        echo $this->content;

        return $this;
    }
	
	/**
     * Sends HTTP headers and content. (Note: This is copied from Symfony 2 codebase)
     *
     * @return Response
     *
     * @api
     */
    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();

        if (function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        } elseif ('cli' !== PHP_SAPI) {
            // ob_get_level() never returns 0 on some Windows configurations, so if
            // the level is the same two times in a row, the loop should be stopped.
            $previous = null;
            $obStatus = ob_get_status(1);
            while (($level = ob_get_level()) > 0 && $level !== $previous) {
                $previous = $level;
                if ($obStatus[$level - 1] && isset($obStatus[$level - 1]['del']) && $obStatus[$level - 1]['del']) {
                    ob_end_flush();
                }
            }
            flush();
        }

        return $this;
    }

    // http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
    /**
     * Is response invalid?
     *
     * @return Boolean
     *
     * @api
     */
    public function isInvalid()
    {
        return $this->statusCode < 100 || $this->statusCode >= 600;
    }
	
}