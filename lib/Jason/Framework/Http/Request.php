<?php
namespace Jason\Framework\Http;

class Request
{
	protected $get, $post, $cookie, $files, $server;
	
	public function __construct($get, $post, $cookie, $files, $server)
	{
		$this->get		= $get;
		$this->post		= $post;
		$this->cookie	= $cookie;
		$this->files	= $files;
		$this->server	= $server;
	}
	
	/**
	 * 
	 * @return \Jason\Framework\Http\Request
	 */
	public static function CreateFromGlobals()
	{
		$request = new static($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
		
		return $request;
	}
	
	public function getRequestUri()
	{
		return $this->server['REQUEST_URI'];
	}
	
	public function getMethod()
	{
		return $this->server['REQUEST_METHOD'];
	}
	
	public function getPost($key = null)
	{
		if (!$key) {
			return $this->post;
		}
		
		return $this->post[$key];
	}
	
	public function getGet($key = null)
	{
		if (!$key) {
			return $this->get;
		}
		
		return $this->get[$key];
	}
	
	public function getCookie($key = null)
	{
		if (!$key) {
			return $this->cookie;
		}
		
		return $this->cookie[$key];
	}
	
	public function getFiles($key = null)
	{
		if (!$key) {
			return $this->files;
		}
		
		return $this->files[$key];
	}
	
	public function getServer($key = null)
	{
		if (!$key) {
			return $this->server;
		}
		
		return $this->server[$key];
	}
}