<?php
namespace Jason\Framework\Session;

use Jason\Framework\Application;

class Session
{
	protected $model;
	protected $user;
	protected $_field_user_id;
	
	protected $_checked = false;
	
	public function __construct(array $settings)
	{
		$this->model = isset($settings['model']) ? $settings['model'] : 'UserInterface';
		$this->_field_user_id = isset($settings['user_id']) ? $settings['user_id'] : 'uid';
	}
	
	public function start()
	{
		session_start();
// 		$_SESSION[$this->_field_user_id] = '1';
	}
	
	public function getUser()
	{
		if (!$this->_checked) {
			if (isset($_SESSION[$this->_field_user_id])) {
				//TODO: get from model
// 				var_dump($_SESSION[$this->_field_user_id]);
				$factory = call_user_func(array($this->model, 'getFactory')); /* @var $factory Jason\Framework\Model\ModelFactory */
				
				$user = $factory->get($_SESSION[$this->_field_user_id]);
				
				if ($user) {
					$this->user = $user;
				}
			}
			
			$this->_checked = true;
		}

		return $this->user;
	}
	
	public function login($id)
	{
		$this->set($this->_field_user_id, $id);
	}
	
	public function logout()
	{
		$this->set($this->_field_user_id);
		unset($this->user);
		$this->_checked = true;
	}
	
	public function get($key, $default = null)
	{
		return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
	}
	
	public function set($key, $value = null)
	{
		if (is_null($value)) {
			unset($_SESSION[$key]);
		} else {
			$_SESSION[$key] = $value;
		}
	}
	
	public function setFlash($key, $value)
	{
		$flashes = $this->get('flashes', array());
		
		$flashes[$key] = $value;
		$this->set('flashes', $flashes);
	}
	
	public function hasFlash($key)
	{
		$flashes = $this->get('flashes', array());
		
		return isset($flashes[$key]);
	}
	
	public function getFlash($key, $default = null)
	{
		$flashes = $this->get('flashes', array());
		$flash = $default;
		
		if (isset($flashes[$key])) {
			$flash = $flashes[$key];
			unset($flashes[$key]);
			
			$this->set('flashes', $flashes);
		}
		
		return $flash;
	}
	
	public function isLoggedIn()
	{
		return !is_null($this->getUser());
	}
	
}