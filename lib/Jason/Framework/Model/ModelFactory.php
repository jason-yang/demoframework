<?php
namespace Jason\Framework\Model;

use Jason\Framework\Database;

use Jason\Framework\Application;

/**
 * Very simple factory to obtain model data from database
 * 
 * @author Jason Yang
 *
 */
class ModelFactory
{
	protected $modelName;
	protected $databaseKey;
	
	public function __construct($model)
	{
		$this->modelName = $model;
		// TODO: Check if model is valid
		
	}
	
	/**
	 * Get item by PK
	 * 
	 * @param mixed $id
	 * @return mixed|NULL
	 */
	public function get($id)
	{
		$modelName = $this->modelName;
		$db = $this->getDatabase();
		
		$sql = sprintf('SELECT %s FROM %s WHERE %s = ?', '*', $modelName::$tableName, $modelName::$primaryKey);
		$stmt = $this->query($sql, array($id));
		
		return $this->fetch($stmt);
	}
	
	/**
	 * Query Database. Returns PDOStatement
	 * @param string $sql
	 * @param array $params
	 * @return \PDOStatement
	 */
	public function query($sql, $params = array())
	{
		$stmt = $this->getDatabase()->prepare($sql);
		$stmt->execute($params);
		
		return $stmt;
	}
	
	public function fetch($stmt, $mode = \PDO::FETCH_ASSOC)
	{
		if ($row = $stmt->fetch($mode)) {
			return $this->rowToModel($row);
		}
		
		return null;
	}
	
	public function fetchAll(\PDOStatement $stmt, $mode = \PDO::FETCH_ASSOC)
	{
		$rows = $stmt->fetchAll($mode);
		
		$models = array();
		foreach ($rows as $row) {
			$models[] = $this->rowToModel($row);
		}
		
		return $models;
	}
	
	protected function rowToModel($row)
	{
		$modelName = $this->modelName;
		$model = new $modelName();
		
		foreach ($row as $key => $val) {
			$model[$key] = $val;
		}
		
		return $model;
	}
	
	/**
	 * 
	 * @return Database
	 */
	protected function getDatabase()
	{
		return Application::instance()->getDatabase($this->databaseKey);
	}
	
	
}