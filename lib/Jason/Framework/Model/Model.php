<?php
namespace Jason\Framework\Model;

/**
 * Model object. This class is *far* from complete
 * @author Jason Yang
 *
 */
class Model implements \ArrayAccess
{
	public static $factoryClass = 'Jason\Framework\Model\ModelFactory';
	
	public static $tableName;
// 	public static $belongsTo = array();
// 	public static $hasMany = array();
// 	public static $hasAndBelongsToMany = array();
	public static $fields = array();
	public static $primaryKey;

// 	protected $_rel_belongsTo = array();
// 	protected $_rel_hasMany = array();
// 	protected $_rel_hasAndBelongsToMany = array();
	
	protected $_data;
	protected $_orig;
	protected $_changed;
	
	public function __construct($data = array())
	{
		$this->_data = $data;
		$this->_orig = $data;
	}
	
	/**
	 * 
	 * @return ModelFactory
	 */
	public static function getFactory()
	{
		$factoryClass = self::$factoryClass;
		return new $factoryClass(get_called_class());
	}
	
	public function save()
	{
		$fields = static::$fields;
		
		if (isset($this->_data[static::$primaryKey])) { // UPDATE
			// TODO:
		} else { // INSERT
			$fieldValues = array();
			foreach ($fields as $k => $v) {
				if ($k == static::$primaryKey) {
					unset($fields[$k]);
				} else {
					$fieldValues[] = sprintf(':%s', $k);
				}
			}
			$fieldNames = array_keys($fields);
			
			$sql = sprintf('INSERT INTO %s (%s) VALUES (%s)', static::$tableName, implode(', ', $fieldNames), implode(', ', $fieldValues));
			
			foreach ($this->_data as $k => $v) {
				if (!isset($fields[$k])) {
					unset($this->_data[$k]);
				}
			}
			
			self::getFactory()->query($sql, $this->_data);
		}
	}
	
	public function delete()
	{
		
	}
	
	/**
	 * @see \ArrayAccess::offsetExists
	 * @param boolean $offset
	 */
	public function offsetExists($offset)
	{
		return isset($this->_data[$offset]);
	}
	
	/**
	 * @see \ArrayAccess::offsetGet
	 * @param mixed $offset
	 */
	public function offsetGet($offset)
	{
		return $this->_data[$offset];
	}
	
	/**
	 * @see \ArrayAccess::offsetSet
	 * @param mixed $offset
	 * @param mixed $value
	 */
	public function offsetSet($offset, $value)
	{
		if (!in_array($offset, array_keys(static::$fields))) return; // Don't set values if not in fields
		
		if (!isset($this->_data[$offset]) || $value != $this->_data[$offset]) {
			$this->_changed[] = $offset;
			$this->_data[$offset] = $value;
		}
	}
	
	/**
	 * @see \ArrayAccess::offsetUnset
	 * @param unknown $offset
	 */
	public function offsetUnset($offset)
	{
		unset($this->_data[$offset]);
	}

}