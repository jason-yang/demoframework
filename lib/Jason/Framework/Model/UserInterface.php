<?php
namespace Jason\Framework\Model;

interface UserInterface
{
	/**
	 * Checks the validity of credentials
	 * 
	 * @param string $username
	 * @param string $password
	 * @return UserInterface
	 */
	public static function checkCredentials($username, $password);
	
	/**
	 * Logs in user if credentials are valid, then returns a UserInterface of the logged in user.
	 * Returns null if credentials are invalid
	 * 
	 * @param string $username
	 * @param string $password
	 * @return UserInterface|Null Returns user if successful, Null otherwise
	 */
	public static function login($username, $password);
	
	/**
	 * Logs out specified user
	 */
	public static function logout();
	
	/**
	 * Set password. Automatically salts and hashes the password.
	 * 
	 * @param string $password
	 */
	public function setPassword($password);
	
	/**
	 * Set password. Raw.
	 * 
	 * @param string $password_raw
	 */
	public function setPasswordRaw($password_raw);
}