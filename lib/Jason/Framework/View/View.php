<?php
namespace Jason\Framework\View;

use Jason\Framework\Controller\Controller;

class View
{
	protected $parent;
	protected $parentRef;
	protected $template;
	protected $params;
	
	public function __construct($params = array())
	{
		$this->params = $params;
		$this->setUp();
	}
	
	public function setUp()
	{
		
	}
	
	public function set($key, $value = null)
	{
		if (is_null($value)) {
			unset($this->params[$key]);
		} else {
			$this->params[$key] = $value;
		}
	}
	
	public function get($key, $default = null)
	{
		return isset($this->params[$key]) ? $this->params[$key] : $default;
	}
	
	public function __toString()
	{
		ob_start();
		extract($this->params);
		require $this->template;
		
		$content = ob_get_contents();
		ob_clean();
		
		if ($this->parent) {
			$params = $this->params;
			$params[$this->parentRef] = $content;
			$parent_template = new $this->parent($params);
			
			$content = (string) $parent_template;
		}
		
		return $content;
	}
}