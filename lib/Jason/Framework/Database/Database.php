<?php
namespace Jason\Framework\Database;

/**
 * Wrapper class for PDO
 * 
 * @author Jason Yang
 *
 */
class Database
{
	protected $pdo;
	
	/**
	 * Uses PDO MySQL only (for now)
	 */
	public function __construct($dsl, $username, $password, $charset = 'utf8')
	{
		$this->pdo = new \PDO($dsl, $username, $password, array(
				\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $charset
				));
	}
	
	public function __call($name, $arguments)
	{
		return call_user_func_array(array($this->pdo, $name), $arguments);
	}
	
}