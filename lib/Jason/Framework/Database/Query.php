<?php
namespace Jason\Framework\Database;

/**
 * INCOMPLETE - NOT USED
 * @author Jason Yang
 *
 */
class Query
{
	const TYPE_SELECT = 1;
	const TYPE_INSERT = 2;
	const TYPE_UPDATE = 3;
	const TYPE_DELETE = 4;
	
	protected $sql;
	
	protected $type;
	protected $select;
	protected $update;
	protected $from;
	protected $where;
	protected $whereParams;
	
	
	
	public function select($fields = null)
	{
		$this->type = self::TYPE_SELECT;
		$this->select = array();
		
		if ($fields) {
			if (is_array($fields)) {
				foreach ($fields as $f) {
					$this->select[] = $f;
				}
			} else {
				$this->select[] = $fields;
			}
		}
	}
	
	public function from($from)
	{
		$this->from = $from;
	}
	
	public function where($where, $params)
	{
		$this->where = $where;
		$this->whereParams = $params;
	}
	
	public function execute()
	{
		
	}
	
	
	
}