<?php
namespace Jason\Framework;

use Jason\Framework\Session\Session;
use Jason\Framework\Http\Request;
use Jason\Framework\Http\Response;
use Jason\Framework\Database\Database;

/**
 * 
 * @author Jason Yang
 */
class Application
{
	protected static $_instance;
	
	/**
	 * @var Configuration
	 */
	protected $configuration;
	
	/**
	 * 
	 * @var Session
	 */
	protected $session;
	
	protected $databases;
	
	protected $routers = array();
	
	/**
	 * 
	 * @return \Jason\Framework\Application
	 */
	public static function instance()
	{
		if (!self::$_instance) {
			self::$_instance = new static();
		}
		
		return self::$_instance;
	}
	
	public static function terminate()
	{
		static::instance()->quit();
	}
	
	/**
	 * Constructor. Singleton
	 */
	protected function __construct()
	{
		$this->setUp();
		
		$this->init();
	}
	
	/**
	 * Initialise application
	 */
	protected function init()
	{
		// TODO: Connect to database(s)
		$this->initDatabases();
		
		// TODO: Setup session (if needed)
		$this->initSession();
	}
	
	protected function initDatabases()
	{
		if ($this->getConfiguration()->getDatabases()) {
			foreach ($this->getConfiguration()->getDatabases() as $key => $db_config) {
				$this->databases[$key] = new Database((string) $db_config->dsl, (string) $db_config->user, (string) $db_config->password);
			}
		}
	}
	
	protected function initSession()
	{
		if ($this->getConfiguration()->getSession()) {
			$this->session = new Session($this->getConfiguration()->getSession());
			$this->session->start();
		}
	}
	
	/**
	 * 
	 * @param Request $request
	 * @return \Jason\Framework\Http\Response
	 */
	public function handle(Request $request)
	{
		foreach ($this->routers as $router) {
			if ($route = $router->match($request)) {
				return $route->execute($request);
			}
		}

		return new Response('Sorry the page you are looking for does not exist.', 404);
	}
	
	/**
	 * Sets up configuration for Application.
	 * Please use this to set up Application independant settings. 
	 */
	public function setUp()
	{
		
	}
	
	/**
	 * Getter for configurtion of application
	 * 
	 * @return \Jason\Framework\Configuration
	 */
	public function getConfiguration()
	{
		return $this->configuration;
	}
	
	/**
	 * Shortcut for getConfiguration
	 * 
	 * @see Application::getConfiguration
	 * @return \Jason\Framework\Configuration
	 */
	public function getConfig()
	{
		return $this->getConfiguration();
	}
	
	/**
	 * 
	 * @return \Jason\Framework\Session\Session
	 */
	public function getSession()
	{
		return $this->session;
	}
	
	/**
	 * 
	 * @param string $key
	 * @return \PDO
	 */
	public function getDatabase($key = null)
	{
		if (!$key) {
			reset($this->databases);
			return current($this->databases);
		}
		
		return $this->databases[$key];
	}
	
	public function quit()
	{
		
		exit;
	}
}