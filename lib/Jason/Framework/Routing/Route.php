<?php
namespace Jason\Framework\Routing;

use Jason\Framework\Configuration;

use Jason\Framework\Http\Response;

use Jason\Framework\Http\Request;

class Route
{
	protected $pattern;
	protected $controller;
	protected $method;
	protected $request_methods = array();
	
	public function __construct($data)
	{
		$this->pattern = (string) $data->pattern;
		$this->controller = (string) $data->controller;
		$this->method = (string) $data->method;
		
		if (isset($data->request_method)) {
			$this->addRequestMethod((string) $data->request_method);
		} else { // Allow all methods
			$this->addRequestMethod(array('GET', 'POST', 'PUT', 'DELETE'));
		}
	}
	
	/**
	 * 
	 * @param string $request_uri
	 * @return boolean
	 */
	public function match(Request $request)
	{
		$path = parse_url($request->getRequestUri(), PHP_URL_PATH);
		$request_method = $request->getMethod();
		
		// Check method first (cheaper)
		if (!$this->isValidRequestMethod($request->getMethod())) {
			return false;
		}
		
		// Check path
		return preg_match($this->pattern, $path) > 0 ? true : false;
	}
	
	public function execute(Request $request)
	{
// 		var_dump($this->controller);
		$controller = new $this->controller($request);
		
		return call_user_func(array($controller, $this->method));
	}
	
	public function addRequestMethod($value)
	{
		if (is_array($value)) {
			foreach ($value as $v) {
				$this->addRequestMethod($v);
			}
		} else {
			$values = explode(',', $value);
			if (count($values) > 1) { // More than 1
				foreach ($values as $v) {
					$this->request_methods[] = strtolower(trim($v));
				}
			} else {
				$this->request_methods[] = strtolower(trim($value));
			}
		}
	}
	
	public function removeRequestMethod($value)
	{
		foreach ($this->request_methods as $k => $v) {
			if ($v == strtolower(trim($value))) {
				unset($this->request_methods[$k]);
			}
		}
	}
	
	public function isValidRequestMethod($method)
	{
		foreach ($this->request_methods as $v) {
			if ($v == strtolower(trim($method))) {
				return true;
			}
		}
		
		return false;
	}
}