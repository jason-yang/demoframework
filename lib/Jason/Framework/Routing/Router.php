<?php
namespace Jason\Framework\Routing;

use Jason\Framework\Http\Request;

class Router
{
	protected $routes = array();
	
	public function __construct($filename)
	{
		$xml = simplexml_load_file($filename, 'SimpleXMLElement', LIBXML_NOCDATA);
		
		foreach ($xml->route as $k => $route) {
			$this->routes[(string) $route['id']] = new Route($route); 
		}
	}
	
	/**
	 * 
	 * @param string $request_uri
	 * @return Route|NULL
	 */
	public function match(Request $request)
	{
		foreach ($this->routes as $route) {
			if ($route->match($request)) {
				return $route;
			}
		}
		
		return null;
	}
}