<?php
namespace Demo\View\Home;

use Jason\Framework\View\View;

class Index extends View
{
	public function setUp()
	{
		$this->parent = 'Demo\\View\\Layout';
		$this->parentRef = 'content';
		$this->template = dirname(__FILE__) . '/../../templates/Home/index.phtml';
	}
}