<?php
namespace Demo\View;

use Jason\Framework\View\View;

class Layout extends View
{
	public function setUp()
	{
		$this->template = dirname(__FILE__) . '/../templates/layout.phtml';
	}
}