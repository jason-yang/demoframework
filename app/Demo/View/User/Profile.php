<?php
namespace Demo\View\User;

use Jason\Framework\View\View;

class Profile extends View
{
	public function setUp()
	{
		$this->parent = 'Demo\\View\\Layout';
		$this->parentRef = 'content';
		$this->template = dirname(__FILE__) . '/../../templates/User/profile.phtml';
	}
}