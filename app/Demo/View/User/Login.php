<?php
namespace Demo\View\User;

use Jason\Framework\View\View;

class Login extends View
{
	public function setUp()
	{
		$this->parent = 'Demo\\View\\Layout';
		$this->parentRef = 'content';
		$this->template = dirname(__FILE__) . '/../../templates/User/login.phtml';
	}
}