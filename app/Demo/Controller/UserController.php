<?php
namespace Demo\Controller;

use Demo\Model\User;

use Jason\Framework\Http\Response;

use Jason\Framework\Controller\Controller;

class UserController extends Controller
{
	public function login()
	{
		return $this->renderView('Demo\View\User\Login', array('title' => $this->getTitle('Login')));
	}
	
	public function login_post()
	{
		$request = $this->getRequest();
		
		if (User::login($request->getPost('username'), $request->getPost('password'))) {
			$this->redirect('/');
		} else {
			$this->getSession()->setFlash('error', 'Invalid username/password');
			return $this->renderView('Demo\View\User\Login', array('title' => $this->getTitle('Login')));
// 			$this->redirect('/login');
		}
	}
	
	public function logout()
	{
		User::logout();
		$this->redirect('/');
	}
	
	public function profile()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->getSession()->setFlash('error', 'Please login first');
			$this->redirect('/login');
		}
		
		return $this->renderView('Demo\View\User\Profile', array('title' => $this->getTitle('Profile'), 'user' => $this->getSession()->getUser()));
	}
	
	public function register()
	{
		if ($this->getSession()->isLoggedIn()) {
			return $this->redirect('/profile');
		}
		
		return $this->renderView('Demo\View\User\Register', array('title' => $this->getTitle('Register')));
	}
	
	public function register_post()
	{
		$request = $this->getRequest();
		
		$filters = array(
				'email' => FILTER_VALIDATE_EMAIL,
				'username' => FILTER_SANITIZE_STRING,
				'password' => FILTER_UNSAFE_RAW,
				'retype_password' => FILTER_UNSAFE_RAW,
		);
		
		$errors = array();
		if ($filtered = filter_var_array($request->getPost(), $filters))
		{
			foreach ($filtered as $key => $value)
			{
				if ($value === false) {
					switch ($key) {
						case 'email':
							$errors[] = 'Please enter a valid Email Address';
							break;
						case 'username':
							$errors[] = 'Please enter an Username';
							break;
						case 'password':
							$errors[] = 'Please enter a password';
							break;
						case 'retype_password':
							// Only check this if password is set
							if ($filtered['password']) {
								$errors[] = 'Please re-enter password';
							}
							break;
					}
				}
			}
			
			if ($filtered['email']) {
				// Check DB
				if (User::checkEmail($filtered['email'])) {
					$errors[] = 'Email Address is already registered';
				}
			}
			
			if ($filtered['username']) {
				$len = strlen ($filtered['username']);
				$min = User::$fields['username']['min'];
				$max = User::$fields['username']['max'];
				if ($len > $max || $len < $min) {
					$errors[] = sprintf('Please enter a username between %s and %d characters', $min, $max);
				}
				
				// Check DB
				if (User::checkUsername($filtered['username'])) {
					$errors[] = 'Username is already registered';
				}
			} else {
				$errors[] = 'Please enter an Username';
			}
			
			if ($filtered['password']) {
				$len = strlen ($filtered['password']);
				$min = User::$fields['password']['min'];
				$max = User::$fields['password']['max'];
				if ($len > $max || $len < $min) {
					$errors[] = sprintf('Please enter a password between %s and %d characters', $min, $max);
					$filtered['password'] = false;
				} else { // Continue to check 
					if ($filtered['password'] != $filtered['retype_password']) {
						$errors[] = 'Please re-type your password';
					}
				}
			} else {
				$errors[] = 'Please enter a password';
			}
		}
		
		if (empty($errors)) {
			$user = new User($filtered);
			$user->setPassword($filtered['password']);
			$user->save();
			
			User::login($filtered['username'], $filtered['password']);
			
			$this->redirect('/profile');
// 			var_dump($user);
// 			var_dump($user['salt'] . $filtered['password']);
// 			var_dump(sha1($user['salt'] . $filtered['password']));
// 			var_dump($filtered['password']);
		} else {
			$this->getSession()->setFlash('errors', $errors);
		}

		return $this->renderView('Demo\View\User\Register', array('title' => $this->getTitle('Register'), 'form_data' => $this->getRequest()->getPost()));
	}
	
}