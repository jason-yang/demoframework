<?php
namespace Demo\Controller;

use Jason\Framework\Controller\Controller;

class HomeController extends Controller
{
	public function index()
	{
		return $this->renderView('Demo\View\Home\Index', array('title' => $this->getTitle('Homepage')));
	}
	
}