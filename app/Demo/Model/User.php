<?php
namespace Demo\Model;

use Jason\Framework\Application;

use Jason\Framework\Model\UserInterface;

use Jason\Framework\Model\Model;

class User extends Model implements UserInterface
{
	public static $tableName = 'users';

	public static $fields = array(
			'id'		=> array('type' => 'integer',	'length' => 10				),
			'username'	=> array('type' => 'string',	'min' => 4,		'max' => 40	),
			'email'		=> array('type' => 'email',		'max' => 127				),
			'password'	=> array('type' => 'password',	'min' => 6,		'max' => 20	),
			'salt'		=> array('type' => 'string',					'max' => 10	),
	);

	public static $primaryKey = 'id';

	public static function checkCredentials($username, $password)
	{
		$factory = self::getFactory();
		
		$sql = sprintf('SELECT * FROM %s WHERE username = ?', static::$tableName);
		$stmt = $factory->query($sql, array($username));
		$salt_user = $factory->fetch($stmt);
		
		$sql = sprintf('SELECT * FROM %s WHERE username = ? AND password = ?', static::$tableName);
		$stmt = $factory->query($sql, array($username, self::getHashedPassword($password, $salt_user['salt'])));
		$user = $factory->fetch($stmt);
		
		return $user;
	}

	public static function login($username, $password)
	{
		$user = self::checkCredentials($username, $password);
		if ($user instanceof UserInterface) {
			Application::instance()->getSession()->login($user['id']);
			return true;
		}
		
		return false;
	}
	
	/**
	 * CHeck if email is already being used
	 * @param string $email
	 * @return boolean
	 */
	public static function checkEmail($email)
	{
		$sql = sprintf('SELECT * FROM %s WHERE email LIKE ?', static::$tableName);
		$stmt = self::getFactory()->query($sql, array(sprintf('%%%s%%', $email)));
		
		return $stmt->rowCount() > 0;
	}
	
	/**
	 * CHeck if username is already being used
	 * @param string $username
	 * @return boolean
	 */
	public static function checkUsername($username)
	{
		$sql = sprintf('SELECT * FROM %s WHERE username LIKE ?', static::$tableName);
		$stmt = self::getFactory()->query($sql, array(sprintf('%%%s%%', $username)));
		
		return $stmt->rowCount() > 0;
	}

	public static function logout()
	{
		Application::instance()->getSession()->logout();
	}
	
	public function setPassword($password)
	{
		$this->_data['salt'] = static::generateSalt();
		
		$this->_data['password'] = static::getHashedPassword($password, $this->_data['salt']);
	}
	
	public function setPasswordRaw($password_raw)
	{
		$this['password'] = $password_raw;
	}
	
	protected static function getHashedPassword($password, $salt)
	{
		return sha1($salt . $password);
	}

	protected static function generateSalt()
	{
		$allowedCharacters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$maxIndex = strlen($allowedCharacters) - 1;
		
		$salt = '';
		for ($i = 0; $i < self::$fields['salt']['max']; $i++) {
			$salt .= substr($allowedCharacters, mt_rand(0, $maxIndex), 1);
		}
		
		return $salt;
	}

}
