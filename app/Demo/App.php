<?php
namespace Demo;

use Jason\Framework\Routing\Router;

use Jason\Framework\Configuration;

use Jason\Framework\Application;

class App extends Application
{
	public function setUp()
	{
		$this->configuration = new Configuration('default', dirname(__FILE__) . '/config/config.xml');
		
		$this->routers[] = new Router(dirname(__FILE__) . '/config/routing.xml');
	}
}